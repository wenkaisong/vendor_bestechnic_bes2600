/*
 * Copyright (c) 2023 Bestechnic (Shanghai) Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dm_test.h"

#include "cJSON.h"
#include "cmsis_os2.h"
#include "device_manager_impl_lite_m.h"
#include "los_compiler.h"
#include "los_config.h"
#include "los_sem.h"
#include "ohos_init.h"
#include "parameter.h"
#include "securec.h"

#include <stdlib.h>
#include <string.h>

static const char * const DEMO_PKG_NAME = "com.ohos.devicemanagerdemo";

void onTargetOnline(const DmDeviceBasicInfo *deviceInfo)
{
    DMLOGE("dm demo onTargetOnline.");
}

void onTargetOffline(const DmDeviceBasicInfo *deviceInfo)
{
    DMLOGE("dm demo onTargetOffline.");
}

void onAdvertisingResult(const int advertisingId, const int result)
{
    DMLOGE("dm demo onAdvertisingResult.");
}

void onBindResult(const char *networkId, const int retCode)
{
    DMLOGE("dm demo onBindResult.");
}

void onTargetFound(const DmDeviceBasicInfo *deviceInfo)
{
    DMLOGE("dm demo onTargetFound.");

    char *bindParam = "{\"bindType\":0}";
    OnBindResult cb;
    cb.onBindResult = onBindResult;
    BindTarget(DEMO_PKG_NAME, deviceInfo->deviceId, bindParam, cb);
}

static void DeviceManagerTest(void)
{
    printf("[%s:%d]: %s\n", __FILE__, __LINE__, __func__);

    osThreadAttr_t attr;
    attr.name = "devicemanager task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 65536;
    attr.priority = 24;

    if (osThreadNew((osThreadFunc_t)dm_test, NULL, &attr) == NULL) {
        printf("Failed to create devicemanager task!\n");
    }
}

APP_FEATURE_INIT(DeviceManagerTest);

static void dm_test(void)
{
    DMLOGE("dm demo start.");
    InitDeviceManager();
    char *extra = NULL;
    DevStatusCallback statusCb;
    statusCb.onTargetOnline = onTargetOnline;
    statusCb.onTargetOffline = onTargetOffline;
    RegisterDevStateCallback(DEMO_PKG_NAME, extra, statusCb);
    char *advParam = "{\"pkgName\":\"com.ohos.devicemanagerdemo\"}";
    OnAdvertisingResult advertisingCb;
    advertisingCb.onAdvertisingResult = onAdvertisingResult;
    StartAdvertising(advParam, NULL, advertisingCb);
    char *filterOption = NULL;
    OnTargetFound targetFoundCb;
    targetFoundCb.onTargetFound = onTargetFound;
    char *discoverParam = "{\"pkgName\":\"com.ohos.devicemanagertest\","
                           "\"subscribeId\":123}";
    StartDiscovering(discoverParam, filterOption, targetFoundCb);
}