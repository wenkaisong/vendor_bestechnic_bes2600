/*
 * Copyright (c) 2023 Bestechnic (Shanghai) Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dslm_test.h"
#include "device_security_info.h"
#include "device_security_defines.h"
#include "utils_log.h"

#include "cmsis_os2.h"
#include "cmsis_os2.h"
#include "ohos_init.h"
#include "ohos_types.h"
#include "securec.h"

#define DSLM_TEST_SLEEP_TIME 30
#define DSLM_TEST_STACK_SIZE 0xFFFF
#define DSLM_TEST_PRIORITY 24

static void DefaultDeviceSecurityInfoCallback(const DeviceIdentify *identify, struct DeviceSecurityInfo *info)
    {
        int32_t level = 0;
        // 从设备安全等级信息中提取设备安全等级字段
        int32_t ret = GetDeviceSecurityLevelValue(info, &level);
        if (ret != 0) {
            SECURITY_LOG_ERROR("DslmTest: GetDeviceSecurityLevelValue error");
            // 获取信息失败。此场景建议开发者根据实际情况进行重试
            FreeDeviceSecurityInfo(info);
            return;
        }

        // 成功获取到设备安全等级，确认当前操作允许的最低安全等级
        // 假设当前操作允许的最低设备安全等级为3
        SECURITY_LOG_INFO("DslmTest: GetDeviceSecurityLevelValue = %d", level);

        // 结束处理前，需要释放内存
        FreeDeviceSecurityInfo(info);
    }

    void CheckDestDeviceSecurityLevelAsync()
    {
        SECURITY_LOG_INFO("DslmTest: CheckDestDeviceSecurityLevelAsync");
        DeviceIdentify device = {DEVICE_ID_MAX_LEN, {0}};
        // 调用异步接口获取设备设备的安全等级等级信息
        int ret = RequestDeviceSecurityInfoAsync(&device, NULL, DefaultDeviceSecurityInfoCallback);
        if (ret != 0) {
            SECURITY_LOG_ERROR("DslmTest: CheckDestDeviceSecurityLevelAsync: RequestDeviceSecurityInfoAsync failed");
            // 获取信息失败，此场景建议开发者根据实际情况进行重试
            // 此场景下callback不会回调。
            return;
        }
        SECURITY_LOG_INFO("DslmTest: CheckDestDeviceSecurityLevelAsync success, waiting callback");
        // 调用成功，等待callback回调。
    }

static void DslmTest(void)
{
    SECURITY_LOG_INFO("DslmTest sleep for %{public}us", DSLM_TEST_SLEEP_TIME);
    sleep(DSLM_TEST_SLEEP_TIME);
    SECURITY_LOG_INFO("DslmTest wake up");
    CheckDestDeviceSecurityLevelAsync();
    SECURITY_LOG_INFO("DslmTest finish");
}

static void DeviceSecurityLevelManagerTest(void)
{
    printf("DslmTest initializing...\n");
    SECURITY_LOG_INFO("DslmTest initializing...");
    osThreadAttr_t attr;
    attr.name = "dslmTestTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = DSLM_TEST_STACK_SIZE;
    attr.priority = DSLM_TEST_PRIORITY;

    if (osThreadNew((osThreadFunc_t)DslmTest, NULL, &attr) == NULL) {
        SECURITY_LOG_ERROR("DslmTes: Failed to create DslmTest task!");
    }
}

APP_FEATURE_INIT(DeviceSecurityLevelManagerTest);